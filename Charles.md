## Charles Dickens British novelist

Charles Dickens was born 7 February 1812 and died in 1870. He wrote classic novels of the Victorian era like Great Expectations, David Copperfield, Oliver Twist and A Christmas Carol.The work of Charles Dickens (1812 – 1870) Includes A Christmas Carol, Oliver Twist, and Great Expectations. This short biography tells about his work and little-known aspects of his life.

For example, in June of 1837 something happened that only occurred once in Dickens’s career. He missed a deadline. He was writing two serialized novels at once, The Pickwick Papers and Oliver Twist. However in June of 1837, there was no Pickwick. There was no Oliver Twist. Instead there was a funeral.

Dickens’s sister-in-law, Mary Hogarth was living with Charles and his wife. Mary was a favorite with the couple and was like a little sister to Charles. On the evening of May 6th Mary went with the couple to the St. James Theatre. Everything seemed fine. The group returned late in the evening and Mary retired for the night. Shortly after that Dickens heard a cry from Mary’s room. She was ill. Despite her doctor’s care Mary passed away in Dickens’s arms the next day.
